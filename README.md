# README #
This is my playground for Javascript Labriries. 
The "pawn" is very small client app which I had to do in one of my job interview. 
Rules are simple - Play button move the pawn on board with interval of 1 second. Stop button stops moving. And click on one of the fields puts the pawn to the clicked field.

# Examples #
* Vanila JS
* JQuery
* React
* React + Redux
* Backbone
* Angular 1.6.5
* RxJS

# Planned #
* Angular 4
* Vue.js
* SAPUI5

### How do I get set up? ###
* Clone, npm install, run http-server in root directory and open corresponding folder.